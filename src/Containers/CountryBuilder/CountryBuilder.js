import React, {Component, Fragment} from 'react';
import axios from 'axios';
import CountryData from "../../Components/CountryBuilder/CountryData/CountryData";
import CountryList from "../../Components/CountryBuilder/CountryList/CountryList";

class CountryBuilder extends Component {

  state = {
    countryList: [],
    currentCountry: '',
    bordersName: []
  };

  getCountryData = name => {
    let foundCountry = this.state.countryList.find(country => country.name === name);
    this.setState({currentCountry: foundCountry});
    if (foundCountry) {
      axios.get('https://restcountries.eu/rest/v2/alpha/' + foundCountry.alpha3Code)
        .then(response => Promise.all(response.data.borders.map(border => axios.get('https://restcountries.eu/rest/v2/alpha/' + border))))
        .then(countries => {
          let bordersName = countries.map(country => country.data.name);
          this.setState({bordersName});
        });
    }
  };

  componentDidMount() {
    axios.get('https://restcountries.eu/rest/v2/all').then(resp => {
      this.setState({countryList: resp.data});
    })
  }

  render() {
    return(
      <div style={{overflow: 'hidden'}}>
        <CountryList list={this.state.countryList}
                     clicked={this.getCountryData}/>
        <CountryData country={this.state.currentCountry}
                     borders={this.state.bordersName}/>
      </div>
    );
  }
}

export default CountryBuilder;