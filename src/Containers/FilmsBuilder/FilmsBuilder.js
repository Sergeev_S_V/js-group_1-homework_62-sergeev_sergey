import React, { Component } from 'react';
import AddForm from "../../Components/Films/AddForm";
import WatchList from "../../Components/Films/WatchList";
import './FilmsBuilder.css';

class FilmsBuilder extends Component {

  state = {
    newFilms: [],
    filmName: ''
  };

  currentName = (event) => {
    this.setState({filmName: event.target.value});
  };

  idFilm = 1;

  addNewFilm = () => {
    let newFilms = [...this.state.newFilms];
    let newFilm = {name: this.state.filmName, id: this.idFilm};
    this.state.filmName.length > 0 ? newFilms.push(newFilm) : null;

    this.setState({newFilms});
    localStorage.setItem('newFilms', JSON.stringify(newFilms));
    this.idFilm++;
  };

  removeFilm = (id) => {
    let index = this.state.newFilms.findIndex(film => film.id === id);
    let newFilms = [...this.state.newFilms];
    newFilms.splice(index, 1);

    this.setState({newFilms});
    localStorage.setItem('newFilms', JSON.stringify(newFilms));
  };

  changeName = (event, index) => {
    let newFilms = [...this.state.newFilms];
    let newFilm = {...this.state.newFilms[index]};
    newFilm.name = event.target.value;
    newFilms[index] = newFilm;

    this.setState({newFilms});
    localStorage.setItem('newFilms', JSON.stringify(newFilms));
  };

  componentDidMount() {
    const newFilms = JSON.parse(localStorage.getItem('newFilms'));
    newFilms ? this.setState({newFilms}) : null;
  }

  render() {
    return (
      <div className="FilmsBuilder">
        <AddForm name={this.currentName}
                 click={this.addNewFilm}/>
        <WatchList films={this.state.newFilms}
                   remove={this.removeFilm}
                   change={this.changeName}
        />
      </div>
    );
  }
}

export default FilmsBuilder;
