import React from 'react';
import './ShortInfo.css';

const ShortInfo = props => {
  return(
    <div className='ShortInfo'>
      <p><span style={{fontWeight: 'bold'}}>Time zone: </span> {props.weather.timezone}</p>
      <p><span style={{fontWeight: 'bold'}}>Latitude: </span> {props.weather.latitude}</p>
      <p><span style={{fontWeight: 'bold'}}>Longitude: </span> {props.weather.longitude}</p>
    </div>
  );
};

export default ShortInfo;