import React, {Component} from 'react';

class CurrentlyWeather extends Component {
  render() {
    let timeZone = null;
    if (this.props.weather.timezone) {
      timeZone = (
        <span>{this.props.weather.timezone}</span>
      );
    } else {
      timeZone = (
        <span style={{fontWeight: 'bold'}}>enter the coordinate</span>
      );
    }

    let currentlyWeather = null;
    if (this.props.show) {
      currentlyWeather = (
        <div>
          <p>Chose place is {timeZone}</p>
          <p>Weather is: {this.props.weather.currently.summary}</p>
          <p>Precip type: {this.props.weather.currently.precipType}</p>
          <p>Pressure: {this.props.weather.currently.pressure}</p>
          <p>Temperature: {this.props.weather.currently.temperature}</p>
          <p>Wind speed: {this.props.weather.currently.windSpeed}</p>
          <p>Wind gust: {this.props.weather.currently.windGust}</p>
        </div>
      );
    } else {
      currentlyWeather = (
        <p>Know currently weather: {timeZone}</p>
      );
    }

    let button = null;
    if (!this.props.show) {
      button = (
        <button disabled={!this.props.weather.timezone}
                onClick={this.props.click}>LOOK</button>
      );
    }

    return(
      <div className='CurrentlyWeather'>
        {currentlyWeather}
        {button}
      </div>
    );
  }
}

export default CurrentlyWeather;