import React from 'react';
import ShortInfo from "./ShortInfo/ShortInfo";
import CurrentlyWeather from "./CurrentlyWeather/CurrentlyWeather";
import './WeatherData.css';

const WeatherData = props => {
  return(
    <div className='WeatherData'>
      <ShortInfo weather={props.weather}/>
      <CurrentlyWeather show={props.show}
                        weather={props.weather}
                        click={props.click}/>
    </div>
  );
};

export default WeatherData;
