import React, {Component} from 'react';
import './AddForm.css';


class AddForm extends Component {

  shouldComponentUpdate(nextProps) {
    return nextProps.name !== this.props.name ||
            nextProps.click !== this.props.click;
  }

  render () {
    return (
      <div className="AddForm">
        <input className='EnterText' onChange={this.props.name} type="text"/>
        <button className='Add' onClick={this.props.click}>ADD</button>
      </div>
    );
  }
}

export default AddForm;

