import React, {Component} from 'react';
import './Navigation.css';

class Navigation extends Component {

  showPortfolio = () => {
    this.props.history.push({
      pathname: '/portfolio',
    });
  };


  render(){
    // console.log(this.props);
    return(
      <header>
        <div className='Nav-bar'>
          <ul className="nav nav-tabs">
            <li role="presentation" className="active"><a href="#">Home</a></li>
            <li role="presentation"><a href="#Contacts" className='Contacts'>Contacts</a></li>
            <li role="presentation"><a href="" onClick={this.showPortfolio}
                                       className='Contacts'>Portfolio</a></li>
          </ul>
        </div>
      </header>
    );
  }
}

export default Navigation;