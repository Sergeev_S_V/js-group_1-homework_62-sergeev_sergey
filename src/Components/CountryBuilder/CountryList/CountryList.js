import React from 'react';
import './CountryList.css';
import Country from "./Country";

const CountryList = props => {
  return(
    <div className='CountryList'>
      {props.list.map(country => {
        return (
          <Country key={country.alpha2Code}
                   clicked={() => props.clicked(country.name)}>
            {country.name}
          </Country>
        );
      })}
    </div>
  );
};


export default CountryList;