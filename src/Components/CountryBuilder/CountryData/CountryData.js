import React, {Component, Fragment} from 'react';
import './CountryData.css';

class CountryData extends Component {

  render() {
    let countryData = null;

    if (this.props.country) {
      countryData = (
        <Fragment>
          <h1>{this.props.country.name}</h1>
          <img src={this.props.country.flag} style={{width: '200px', height: 'auto'}} alt=""/>
          <p>Capital: {this.props.country.capital}</p>
          <p>Population: {this.props.country.population}</p>
          <div>
            <p style={{fontWeight: 'bold'}}>Borders with: </p>
            {this.props.borders.length > 0 ? null : 'Nothing'}
            <ul style={{width: '40%', margin: '0 auto'}}>{this.props.borders.length > 0
              ? this.props.borders.map((name, index) => <li style={{marginBottom: '10px'}} key={index}>{name}</li>)
              : null}
            </ul>
          </div>
        </Fragment>
      );
    } else {
      countryData = (
        <p>Choose the country</p>
      );
    }

    return(
      <div className='CountryData'>
        {countryData}
      </div>
    );
  }
}

export default CountryData;