import React, {Component, Fragment} from 'react';
import './Portfolio.css';

class Portfolio extends Component {

  showCountryBuilder = () => {
    this.props.history.push({
      pathname: this.props.match.path + '/countrybuilder'
    });
  };

  showWeatherBuilder = () => {
    this.props.history.push({
      pathname: this.props.match.path + '/weatherbuilder'
    });
  };

  showFilmsBuilder = () => {
    this.props.history.push({
      pathname: this.props.match.path + '/filmsbuilder'
    });
  };

  render() {
    return(
      <div className='chooseForm'>
        <p className='title'>Choose the project</p>
        <div className='chooseProject'>
          <p style={{fontWeight: 'bold'}}>Country builder</p>
          <button onClick={this.showCountryBuilder}>LOOK</button>
        </div>
        <div className='chooseProject'>
          <p style={{fontWeight: 'bold'}}>Weather builder</p>
          <button onClick={this.showWeatherBuilder}>LOOK</button>
        </div>
        <div className='chooseProject'>
          <p style={{fontWeight: 'bold'}}>Films builder</p>
          <button onClick={this.showFilmsBuilder}>LOOK</button>
        </div>
      </div>
    );
  }
}

export default Portfolio;