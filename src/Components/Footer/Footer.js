import React from 'react';
import './Footer.css';

const Footer = () => {
  return(
    <footer>
      <div id='Contacts'>
        <p className='title'>My contacts</p>
        <p>Bishkek, My street 99/7</p>
        <p>e-mail: my-email@mail.ru</p>
        <p>phone: (+996)777-777 777</p>
      </div>
    </footer>
  );
};

export default Footer;