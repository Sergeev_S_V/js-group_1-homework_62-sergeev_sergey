import React, {Component} from 'react';
import './Content.css';

class Content extends Component {
  render() {
    return(
      <div className='Content'>
        <p style={{fontWeight: 'bold'}}>Content</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis mollitia numquam obcaecati odio ratione rerum sunt totam ut! Culpa, enim?</p>
      </div>
    );
  }
}

export default Content;