import React, {Component, Fragment} from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import { withRouter } from 'react-router';
import Portfolio from "./Components/Portfolio/Portfolio";
import Content from "./Components/Content/Content";
import Navigation from "./Components/Navigation/Navigation";
import Footer from "./Components/Footer/Footer";
import WeatherBuilder from "./Containers/WeatherBuilder/WeatherBuilder";
import CountryBuilder from "./Containers/CountryBuilder/CountryBuilder";
import FilmBuilder from "./Containers/FilmsBuilder/FilmsBuilder";

class App extends Component {
  render() {

    return (
      <Fragment>
        <Navigation {...this.props}/>
        <Switch>
          <Route path='/' exact component={Content}/>
          <Route path='/portfolio' exact render={props => <Portfolio {...props} />}/>
          <Route exact path='/portfolio/countrybuilder'
                 component={CountryBuilder}/>
          <Route exact path='/portfolio/weatherbuilder'
                 component={WeatherBuilder}/>
          <Route exact path='/portfolio/filmsbuilder'
                 component={FilmBuilder}/>
          <Route render={() => <h1>404 Not found</h1>} />
        </Switch>
        <Footer/>
      </Fragment>
    );
  }
}

export default withRouter(App);
